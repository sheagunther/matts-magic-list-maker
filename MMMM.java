//////////////////////////////////////////////////////////////////////////////// 100 characters ////
/**
Matt's Magic Match Maker
Code by Shea Gunther, sheagunther@gmail.com
February 2, 2014

   This work is licensed under a Creative Commons 
   Attribution-NonCommercial 4.0 International License.
   http://creativecommons.org/licenses/by-nc/4.0/

 **/


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class MMMM {
  public static void main(String[] args) throws FileNotFoundException {
    int numberOfSessions;
    int numberOfStudents;
    Scanner console = new Scanner(System.in);

    System.out.println("Welcome to Matt's Magic Match Maker! ====================");
    System.out.println("Please enter in the name of the text file to process:");
    System.out.println("(Note: must be a plaintext file (.txt) with an even number " +
        "\nof names, one name per line)");
    String filename = console.nextLine();

    System.out.println("Please enter in how many sessions to generate:");
    String sessionsInput = console.nextLine();
    numberOfSessions = Integer.parseInt(sessionsInput);

    // Prepare to read from the file, using a Scanner object
    File file = new File(filename);
    Scanner nameScanner = new Scanner(file);

    ArrayList<String> leftHandList = new ArrayList();
    ArrayList<String> rightHandList = new ArrayList();

    // Read in and split the list of names into two separate lists, left and right hand
    int counter = 1;
    while (nameScanner.hasNextLine()){
      if(counter%2 != 0){
        leftHandList.add(nameScanner.nextLine());
      }else
        rightHandList.add(nameScanner.nextLine());
      counter++;
    }

    // Print out and then shuffle the right hand list, giving a new unique grouping with the left
    numberOfStudents = counter;
    System.out.println("\nResults:");
    for(int x = 0; x < numberOfSessions; x++){
      System.out.println("Session #" + (x+1));
      
      for(int y = 0; y < numberOfStudents/2; y++){
        System.out.print(leftHandList.get(y) + " & ");
        System.out.println(rightHandList.get(y));
      }
      rightHandList = advanceList(rightHandList);
      System.out.println();
    }

    System.out.println("MMMM was written by Shea Gunther | sheagunther@gmail.com");
    System.out.println("Thank you for playing! ==============================2014");
  }


  public static ArrayList<String> advanceList(ArrayList<String> listToAdvance){
    String lastString = listToAdvance.get(listToAdvance.size()-1);
    ArrayList<String> listToReturn = new ArrayList<String>();

    int sizeOfList = listToAdvance.size();

    for(int x = 0; x < sizeOfList - 1; x++){
      listToReturn.add(listToAdvance.get(x));
    }
    listToReturn.add(0, lastString);

    return listToReturn;
  }
  
  
  // This isn't used because I realized that applying it means that the requirement
  // that no two pairs repeat over the series of sessions was no longer valid. Derp. 
  public static ArrayList<String> shuffleList(ArrayList<String> listToShuffle){
    ArrayList<String> listToReturn = new ArrayList<String>();
    Random randomObject = new Random();
    
    while(listToShuffle.size() > 0){
      int randomInteger = randomObject.nextInt(listToShuffle.size());
      String randomString = listToShuffle.get(randomInteger);
      listToShuffle.remove(randomInteger);
      listToReturn.add(randomString);
    }
   
    return listToReturn;
  }
  
  
  
  
  
  
  
  
}
